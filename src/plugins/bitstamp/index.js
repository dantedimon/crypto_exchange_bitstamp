import {webSocket} from "rxjs/webSocket";
import {map, throttleTime} from "rxjs/operators";
import {BehaviorSubject} from "rxjs";
import {CHANNELS, CURRENCIES} from "./constants";


export class Bitstamp {
    constructor() {
        // main stream
        this.subject = new webSocket(" wss://ws.bitstamp.net");

        // update data
        this.currency = null;
        this.throttleTime = 100;
        this.ticker$ = new BehaviorSubject({});
        this.orders$ = new BehaviorSubject({});
        this.orderbook$ = new BehaviorSubject({});
        this.detailOrderbook$ = new BehaviorSubject({});
        this.fullOrderbook$ = new BehaviorSubject({});

        // constants
        this.CURRENCIES = CURRENCIES;
        this.CHANNELS = CHANNELS;

        // init
        this.init();
    }

    init() {
        this.subject.subscribe(
            event => {
                const channel = event.channel;

                if (channel) {
                    if (channel === CHANNELS.LIVE_TRADES + this.currency) {
                        this.ticker$.next(event.data);
                    } else if (channel === CHANNELS.LIVE_ORDERS + this.currency) {
                        this.orders$.next(event);
                    } else if (channel === CHANNELS.ORDER_BOOK + this.currency) {
                        this.orderbook$.next(event.data);
                    } else if (channel === CHANNELS.DETAIL_ORDER_BOOK + this.currency) {
                        this.detailOrderbook$.next(event.data);
                    } else if (channel === CHANNELS.DIFF_ORDER_BOOK + this.currency) {
                        this.fullOrderbook$.next(event.data);
                    }
                }
            }
        );

        // this.changeCurrency('btcusd')
    }

    changeCurrency(currency) {
        if (this.currency) {
            this.unsubscribe(this.CHANNELS.LIVE_TRADES);
            this.unsubscribe(this.CHANNELS.LIVE_ORDERS);
            this.unsubscribe(this.CHANNELS.ORDER_BOOK);
            this.unsubscribe(this.CHANNELS.DETAIL_ORDER_BOOK);
            this.unsubscribe(this.CHANNELS.DIFF_ORDER_BOOK);
        }

        this.currency = currency;

        this.subscribe(this.CHANNELS.LIVE_TRADES);
        this.subscribe(this.CHANNELS.LIVE_ORDERS);
        this.subscribe(this.CHANNELS.ORDER_BOOK);
        this.subscribe(this.CHANNELS.DETAIL_ORDER_BOOK);
        this.subscribe(this.CHANNELS.DIFF_ORDER_BOOK);
    }

    subscribe(channel) {
        this.subject.next({
            "event": "bts:subscribe",
            "data": {
                "channel": channel + this.currency
            }
        })
    }

    unsubscribe(channel) {
        this.subject.next({
            "event": "bts:unsubscribe",
            "data": {
                "channel": channel + this.currency
            }
        })
    }

    reconnect() {
        this.subject.next({
            "event": "bts:request_reconnect",
            "channel": "",
            "data": ""
        })
    }

    getTicker() {
        return this.ticker$.pipe(
            throttleTime(this.throttleTime),
        )
    }

    getOrders() {
        return this.orders$.pipe(
            throttleTime(this.throttleTime),
        )
    }

    getOrderbook() {
        return this.orderbook$
            .pipe(
                throttleTime(this.throttleTime),
                map(data => {
                    let result = [];

                    if (data.bids) {
                        data.bids.forEach((b, i) => {
                            result.push(
                                {
                                    price: {
                                        bid: data.bids[i][0],
                                        ask: data.asks[i][0],
                                    },
                                    volume: {
                                        bid: data.bids[i][1],
                                        ask: data.asks[i][1],
                                    }
                                }
                            )
                        })
                    }

                    return result
                })
            )
    }

    getDetailOrderbook() {
        return this.detailOrderbook$
            .pipe(
                throttleTime(this.throttleTime),
                map(data => {
                    let result = [];
                    if (data.bids) {
                        data.bids.forEach((b, i) => {
                            if (data.bids[i] && data.asks[i]) {
                                result.push(
                                    {
                                        price: {
                                            bid: data.bids[i][0],
                                            ask: data.asks[i][0],
                                        },
                                        volume: {
                                            bid: data.bids[i][1],
                                            ask: data.asks[i][1],
                                        }
                                    }
                                )
                            }
                        })
                    }

                    return result
                })
            )
    }

    getFullOrderbook() {
        return this.fullOrderbook$
            .pipe(
                throttleTime(this.throttleTime),
                map(data => {
                    let result = [];

                    if (data.bids) {
                        data.bids.forEach((b, i) => {
                            if (data.bids[i] && data.asks[i]) {
                                result.push(
                                    {
                                        price: {
                                            bid: data.bids[i][0],
                                            ask: data.asks[i][0],
                                        },
                                        volume: {
                                            bid: data.bids[i][1],
                                            ask: data.asks[i][1],
                                        }
                                    }
                                )
                            }
                        })
                    }

                    return result
                })
            )
    }

    static install(Vue, options) {
        Vue.prototype.$bitstamp = new Bitstamp()
    }
}