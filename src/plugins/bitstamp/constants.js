export const CURRENCIES = {
    BTCUSD: 'btcusd',
    BTCEUR: 'btceur',
    EURUSD: 'eurusd',
    XRPEUR: 'xrpeur',
    XRPBTC: 'xrpbtc',
    LTCUSD: 'ltcusd',
    LTCEUR: 'ltceur',
    LTCBTC: 'ltcbtc',
    ETHUSD: 'ethusd',
    ETHEUR: 'etheur',
    ETHBTC: 'ethbtc',
    BCHUSD: 'bchusd',
    BDHEUR: 'bcheur',
    BDHBTC: 'bchbtc'
};

export const CHANNELS = {
    LIVE_TRADES: 'live_trades_',
    LIVE_ORDERS: 'live_orders_',
    ORDER_BOOK: 'order_book_',
    DETAIL_ORDER_BOOK: 'detail_order_book_',
    DIFF_ORDER_BOOK: 'diff_order_book_',
}