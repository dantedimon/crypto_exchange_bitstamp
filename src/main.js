import './registerServiceWorker'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueRx from 'vue-rx'
import {Bitstamp} from "./plugins/bitstamp";

Vue.use(VueRx);
Vue.use(Bitstamp.install);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
