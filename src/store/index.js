import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currency: 'bchusd',
    widgets: {
      ticker: true,
      orders: true,
      orderbook: true,
      detailOrderbook: true,
      fullOrderbook: true
    }
  },
  mutations: {
    setCurrency (state, currency) {
      state.currency = currency
    },
    toggleWidget (state, {name, value}) {
      state.widgets[name] = value
    }
  },
  actions: {
  },
  modules: {
  }
})
